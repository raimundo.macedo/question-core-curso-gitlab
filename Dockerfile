FROM registry.gitlab.com/raimundo.macedo/question-core-curso-gitlab:dependencies

COPY . .

RUN mkdir -p assets/static \
    && python manage.py collectstatic --noinput